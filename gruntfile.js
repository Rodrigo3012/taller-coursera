module.exports = function (grunt){
	grunt.initconfig({
		sass:{
			dist: {
				files: [{
					expand: true,
					cwd: 'css'
					src:['*.scss'],
					dest:'css',
					ext: '.css'
				}]
			}
		},
		watch:{
			files:['css/*.scss'],
			loadNpmTasks: ['css']
		},
		browserSync: {
			dev: {
				bsFiles: { //browser files
					src: [
						'css/*.css',
						'*.html',
						'js/*.js'
						]
					},
				options: {
					watchTask: true,
					server: {
						baseDir: './' //Directorio base para muesto servidor
					}
				}	
			}
		},
		imageemin: {
			dynamic: {
				files:[{
					expand: true,
					cwd: './',
					src: 'images/*.{png,git,jpg,jpeg}',
					dest: 'dist/'
				}]
			}
		},
	});
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.registerTask('css', ['sass']);
	grunt.registerTask('default', ['browserSync', 'watch']);
	grunt.registerTask('img:compress', ['imagemin']);
};